package com.javibelloso.data.repository

import com.javibelloso.data.source.NetworkDataSource

class NetworkRepository(
    private val networkDataSource: NetworkDataSource
) {
    suspend fun manageNetworkManager(lifecycle: Any, listener: (Boolean) -> Unit) =
        networkDataSource.manageNetworkManager(lifecycle, listener)
}
