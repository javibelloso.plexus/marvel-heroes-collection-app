package com.javibelloso.data.repository

import com.javibelloso.data.source.RemoteDataSource
import com.javibelloso.domain.HeroResult

class MarvelRepository(
    private val remoteDataSource: RemoteDataSource
) {
    suspend fun getHeroesCollection(page: Int): List<HeroResult> {
        return remoteDataSource.getHeroesCollection(page)
    }

    suspend fun getHeroDetailById(id: Int): HeroResult {
        return remoteDataSource.getDetailHero(id)
    }

}