package com.javibelloso.data.source

import com.javibelloso.domain.HeroResult

interface RemoteDataSource {
    suspend fun getHeroesCollection(page: Int): List<HeroResult>
    suspend fun getDetailHero(id: Int): HeroResult
}