import com.javibelloso.data.repository.MarvelRepository
import com.javibelloso.data.source.RemoteDataSource
import com.javibelloso.domain.*
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever


@RunWith(MockitoJUnitRunner::class)
class MarvelRepositoryTest {

    @Mock
    lateinit var remoteDataSource: RemoteDataSource

    private val mockedItemData = ItemData(
        "Name",
        "ResourceURI",
        "Type"
    )

    private val mockedSubData = SubData(
        "Available",
        "CollectionURI",
        listOf(mockedItemData),
        "Returned"
    )


    private val mockedHero = HeroResult(
        id = 1011334,
        name = "3-D Man",
        description = "He loves 3-D movies!",
        "Modified",
        "ResourceURI",
        thumbnail = Thumbnail(
            path = "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
            extension = "jpg"
        ),
        mockedSubData
    )

    private val mockedHeroData = Data(
        20,
        20,
        0,
        listOf(mockedHero),
        20
    )


    private val mockedResponse = ResponseHeroesCollection(
        code = 200,
        status = "Ok",
        mockedHeroData
    )

    lateinit var marvelRepository: MarvelRepository

    @Before
    fun setUp() {
        marvelRepository = MarvelRepository(remoteDataSource)
    }

    @Test
    fun getRemoteHeroesCollectionAPI(): Unit = runBlocking {
        whenever(remoteDataSource.getHeroesCollection(0)).thenReturn(mockedResponse.data.results)
        val result = marvelRepository.getHeroesCollection(0)
        assertEquals(mockedResponse.data.results, result)
    }

    @Test
    fun getDetailsHeroByIdAPI(): Unit =
        runBlocking {
            whenever(remoteDataSource.getDetailHero(0)).thenReturn(mockedHero)
            val result = marvelRepository.getHeroDetailById(0)
            assertEquals(mockedHero, result)
        }

}
