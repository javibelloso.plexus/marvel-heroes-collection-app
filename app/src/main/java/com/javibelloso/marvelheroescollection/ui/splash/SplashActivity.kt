package com.javibelloso.marvelheroescollection.ui.splash

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.javibelloso.marvelheroescollection.R
import com.javibelloso.marvelheroescollection.common.startActivity
import com.javibelloso.marvelheroescollection.ui.home.HomeActivity

class SplashActivity : AppCompatActivity() {

    private var TIME_OUT: Long = 3000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadSplashScreen()
    }

    private fun loadSplashScreen() {
        Handler().postDelayed({
            startActivity<HomeActivity>()
        }, TIME_OUT)
    }

}