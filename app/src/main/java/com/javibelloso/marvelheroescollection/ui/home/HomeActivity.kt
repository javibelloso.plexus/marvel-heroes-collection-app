package com.javibelloso.marvelheroescollection.ui.home

import android.app.Activity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.javibelloso.marvelheroescollection.R
import com.javibelloso.marvelheroescollection.databinding.ActivityHomeBinding
import com.test.usecase.GetNetworkManager
import com.javibelloso.marvelheroescollection.common.startActivity
import com.javibelloso.marvelheroescollection.server.ServiceLocator
import com.javibelloso.marvelheroescollection.ui.detail.DetailActivity
import com.javibelloso.marvelheroescollection.ui.home.adapter.HeroesListAdapter
import com.javibelloso.marvelheroescollection.ui.home.adapter.paging.ResultsLoadStateAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest


@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    private val networkRepository by lazy {
        ServiceLocator.provideNetworkRepository(
            applicationContext
        )
    }

    private lateinit var binding: ActivityHomeBinding
    private val adapter = HeroesListAdapter {
            startActivity<DetailActivity>(DetailActivity.CHARACTER_ID to it.id)
    }
    private lateinit var alertDialog: AlertDialog


    private val viewModel: HomeViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getNetworkManager()
        adapterCustom((1..4).random())
        initList()
        getListeners()

    }

    fun getListeners() {
        with(binding){
            btnRefresh.setOnClickListener {
                initList()
            }
            btOneColumns.setOnClickListener { view -> adapterCustom(1) }
            btTwoColumns.setOnClickListener { view -> adapterCustom(2)  }
            btThreeColumns.setOnClickListener { view -> adapterCustom(3)  }
            btFourColumns.setOnClickListener { view -> adapterCustom(4)  }
        }

    }

    private fun adapterCustom(columns:Int) {
        val layoutManager = GridLayoutManager(this@HomeActivity, columns)
        val footerAdapter = ResultsLoadStateAdapter(adapter::retry)

        binding.apply {
            mainHeroList.layoutManager = layoutManager
            mainHeroList.adapter = adapter.withLoadStateFooter(footerAdapter)
        }

            layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (position == adapter.itemCount && footerAdapter.itemCount > 0) {
                        columns
                    } else {
                        1
                    }
                }
            }
    }

    fun initList() {
        val footerAdapter = ResultsLoadStateAdapter { adapter.retry() }
        binding.mainHeroList.adapter = adapter.withLoadStateFooter(footerAdapter)
        lifecycleScope.launchWhenStarted {
            viewModel.apiData().collectLatest {
                    it.let {
                        adapter.submitData(it)
                    }
            }
        }

        adapter.addLoadStateListener {
            when(it.refresh){
                is LoadState.Error -> noData(it.refresh as Any,this@HomeActivity)
                }
            binding.pb.isVisible = it.refresh is LoadState.Loading
            }
    }
    private fun noData(messageError: Any,activity:Activity) {
        alertDialog = activity.let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setPositiveButton(R.string.ok) { dialog, id ->
                    // no-op
                }
            }
            builder.setTitle("Error")
            builder.setMessage("Server error")
            builder.create()
        }
        alertDialog?.show()
    }
    //  Internet Connection
    private fun getNetworkManager() {
        lifecycleScope.launchWhenStarted {
            GetNetworkManager(networkRepository).run(lifecycle, ::shouldShowOfflineIcon)
        }
    }

    /**
     * If the visibility suddenly changes when we had already changed it a few seconds ago,
     * we need to wait to avoid race conditions.
     */
    private fun shouldShowOfflineIcon(internetAvailable: Boolean) {
        lifecycleScope.launchWhenStarted {
            delay(200)
            binding.icInternet.isVisible = !internetAvailable

        }
    }


}