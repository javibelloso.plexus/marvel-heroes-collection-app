package com.javibelloso.marvelheroescollection.ui.home.adapter.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.javibelloso.domain.HeroResult
import com.javibelloso.marvelheroescollection.api.DEFAULT_PAGE_INDEX
import com.javibelloso.marvelheroescollection.api.VALUE_COUNT_PAGE
import com.javibelloso.usecase.GetHeroes
import retrofit2.HttpException
import java.io.IOException

class ResultPagingSource(
    val getHeroes: GetHeroes
) : PagingSource<Int, HeroResult>() {

    override suspend fun load(
        params: LoadParams<Int>
    ): LoadResult<Int, HeroResult> {

        return try {
                        val offset = params.key ?:  DEFAULT_PAGE_INDEX - 1
                        val response = getHeroes.run(offset)
                        val results = response
                        LoadResult.Page(
                            data = results,
                            prevKey = null,
                            nextKey = if (results!!.isEmpty()) null else offset + VALUE_COUNT_PAGE
                        )
                    } catch (e: IOException) {
                        LoadResult.Error(e)
                    } catch (e: HttpException) {
                        LoadResult.Error(e)
                    }
    }

    override fun getRefreshKey(state: PagingState<Int, HeroResult>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(DEFAULT_PAGE_INDEX) ?: anchorPage?.nextKey?.minus(DEFAULT_PAGE_INDEX)
        }
    }

}
