package com.javibelloso.marvelheroescollection.ui.detail

import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.javibelloso.domain.HeroResult
import com.javibelloso.marvelheroescollection.common.ScopedViewModel
import com.javibelloso.usecase.GetHeroDetail
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named

@HiltViewModel
class DetailViewModel @Inject constructor(
    @Named("characterId") private val characterId: Int,
    private val getHeroDetail: GetHeroDetail
) : ScopedViewModel() {

    private val _model = MutableLiveData<HeroResult>()
    val model: LiveData<HeroResult> = _model

    private val _load = MutableLiveData<Boolean>()
    val load: LiveData<Boolean> = _load

    private val _error = MutableLiveData<Boolean>()
    val error: LiveData<Boolean> = _error

    init {
        getDetails()
    }

    private fun getDetails() {
        viewModelScope.launch {
            _load.value = true
            _error.value = false
            try {
                _model.value = getHeroDetail.run(characterId)
                _load.value = false
            }catch (e:Exception){
                _load.value = false
                _error.value = true
            }

        }
    }

}