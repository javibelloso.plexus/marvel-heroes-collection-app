package com.javibelloso.marvelheroescollection.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.javibelloso.domain.HeroResult
import com.javibelloso.marvelheroescollection.R
import com.javibelloso.marvelheroescollection.databinding.CardHeroItemBinding
import com.javibelloso.marvelheroescollection.common.loadImage

class HeroesListAdapter(private val clientsClickListener: (HeroResult) -> Unit) :
    PagingDataAdapter<HeroResult, HeroesListAdapter.ViewHolder>(DiffCallback) {

    init {
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    companion object DiffCallback : DiffUtil.ItemCallback<HeroResult>() {
        override fun areItemsTheSame(oldItem: HeroResult, newItem: HeroResult) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: HeroResult, newItem: HeroResult) =
            oldItem == newItem
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = CardHeroItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val client = getItem(position)
        if (client != null) {
            holder.bind(client)
        }
        holder.itemView.setOnClickListener { clientsClickListener(client!!) }
    }


    class ViewHolder(private val binding: CardHeroItemBinding) :
        RecyclerView.ViewHolder(binding.root) {


        fun bind(mediaAccount: HeroResult) {
            binding.heroText.text = mediaAccount.name
            if (mediaAccount.thumbnail.path.contains("not_available")) {
                binding.heroImage.setImageResource(R.drawable.marvel)
            } else {
                mediaAccount.thumbnail?.path?.let {
                    binding.heroImage.loadImage(
                        it,
                        mediaAccount.thumbnail?.extension
                    )
                }
            }
        }

    }
}
