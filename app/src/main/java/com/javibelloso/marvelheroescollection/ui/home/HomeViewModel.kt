package com.javibelloso.marvelheroescollection.ui.home

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.javibelloso.domain.HeroResult
import com.javibelloso.marvelheroescollection.common.ScopedViewModel
import com.javibelloso.marvelheroescollection.ui.home.adapter.paging.ResultPagingSource
import com.javibelloso.usecase.GetHeroes
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class HomeViewModel
    @Inject constructor(
    private val getHeroes: GetHeroes
) : ScopedViewModel() {


    fun apiData(): Flow<PagingData<HeroResult>> {
        return Pager(
            config = PagingConfig(
                pageSize = 18,
                enablePlaceholders = true//,
                //     maxSize = 30,
                //     prefetchDistance = 5,
                //     initialLoadSize = 10
            ),
            pagingSourceFactory = { ResultPagingSource(getHeroes) }
        ).flow
    }

}