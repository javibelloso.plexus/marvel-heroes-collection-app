package com.javibelloso.marvelheroescollection.ui.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.javibelloso.domain.HeroResult
import com.javibelloso.marvelheroescollection.databinding.ActivityDetailBinding
import com.javibelloso.marvelheroescollection.common.loadImage
import com.javibelloso.marvelheroescollection.common.showErrorDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailActivity : AppCompatActivity() {

    companion object {
        const val CHARACTER_ID = "DetailActivity:id"
    }


    private lateinit var binding: ActivityDetailBinding
    private val viewModel: DetailViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.load.observe(this, Observer {
            binding.pb.isVisible = if (it) true else false })
        viewModel.model.observe(this,
            Observer {
                    getDatas(it)
            })
        viewModel.error.observe(this, Observer {
            if(it){
                val dialogError = AlertDialog.Builder(this@DetailActivity)
                dialogError.showErrorDialog("Server error") {
                    finish()
                }
                dialogError.show()

            }})
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    fun getDatas(hero: HeroResult) {
        binding.toolbarLayout.title = hero.name
        binding.toolbarImage.loadImage(
            hero.thumbnail.path,
            hero.thumbnail.extension
        )
        binding.heroName.text = hero.name
        binding.heroContent.text = if(hero.description.length>0) hero.description else "This Hero does't contain description"

    }

}


