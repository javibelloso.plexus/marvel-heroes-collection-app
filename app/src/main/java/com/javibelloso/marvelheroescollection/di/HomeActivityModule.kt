package com.javibelloso.marvelheroescollection.di

import com.javibelloso.data.repository.MarvelRepository
import com.javibelloso.usecase.GetHeroes
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped


@Module
@InstallIn(ViewModelComponent::class)
class HomeActivityModule {

    @ViewModelScoped
    @Provides
    fun provideGetHeroes(marvelRepository: MarvelRepository): GetHeroes = GetHeroes(marvelRepository)

}