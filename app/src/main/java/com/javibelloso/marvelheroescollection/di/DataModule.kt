package com.javibelloso.marvelheroescollection.di

import com.javibelloso.data.repository.MarvelRepository
import com.javibelloso.marvelheroescollection.api.TheMarvelDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataModule {

    @Singleton
    @Provides
    fun repositoryProvider(
        remoteDataSource: TheMarvelDataSource
    ): MarvelRepository = MarvelRepository(remoteDataSource)

}