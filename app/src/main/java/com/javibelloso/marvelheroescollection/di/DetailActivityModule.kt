package com.javibelloso.marvelheroescollection.di

import androidx.lifecycle.SavedStateHandle
import com.javibelloso.data.repository.MarvelRepository
import com.javibelloso.marvelheroescollection.ui.detail.DetailActivity
import com.javibelloso.usecase.GetHeroDetail
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import java.lang.IllegalStateException
import javax.inject.Named

@Module
@InstallIn(ViewModelComponent::class)
class DetailActivityModule {


    @Provides
    @Named("characterId")
    fun characterIdProvider(stateHandle: SavedStateHandle): Int =
        stateHandle.get<Int>(DetailActivity.CHARACTER_ID)
            ?: throw IllegalStateException("character id not found in the state handle")


    @ViewModelScoped
    @Provides
    fun provideGetDetails(marvelRepository: MarvelRepository): GetHeroDetail = GetHeroDetail(marvelRepository)

}