package com.javibelloso.marvelheroescollection.server

import android.content.Context
import com.javibelloso.data.repository.NetworkRepository
import com.javibelloso.marvelheroescollection.api.network.NetworkDataSourceImpl

object ServiceLocator {

    fun provideNetworkRepository(
        context: Context
    ): NetworkRepository {
        return NetworkRepository(
            NetworkDataSourceImpl(context) )
    }

}
