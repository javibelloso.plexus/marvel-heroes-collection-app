package com.javibelloso.marvelheroescollection.api.model


data class ResponseHeroes(
    val code: Int,
    val status: String,
    val `data`: Data
)