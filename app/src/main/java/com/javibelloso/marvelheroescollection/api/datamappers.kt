package com.javibelloso.marvelheroescollection.api

import com.javibelloso.marvelheroescollection.api.model.SubData as ServerSubData
import com.javibelloso.marvelheroescollection.api.model.Data as ServerData
import com.javibelloso.marvelheroescollection.api.model.ResponseHeroes as ServerResponse
import com.javibelloso.marvelheroescollection.api.model.Thumbnail as ServerThumbnail
import com.javibelloso.marvelheroescollection.api.model.HeroResult as ServerHero
import com.javibelloso.marvelheroescollection.api.model.ItemData as ServerItemData

import com.javibelloso.domain.SubData as DomainSubData
import com.javibelloso.domain.Data as DomainData
import com.javibelloso.domain.ResponseHeroesCollection as DomainResponse
import com.javibelloso.domain.Thumbnail as DomainThumbnail
import com.javibelloso.domain.HeroResult as DomainHero
import com.javibelloso.domain.ItemData as DomainItemData


fun DomainResponse.toServerResponse(): ServerResponse = ServerResponse(
    code,
    status,
    data.toServerData()
)

fun DomainData.toServerData(): ServerData = ServerData(
    count,
    limit,
    offset,
    results.map { it.toServerHero() },
    total
)

fun DomainHero.toServerHero(): ServerHero = ServerHero(
    id,
    name,
    description,
    modified,
    resourceURI,
    thumbnail.toServerThumbnail(),
    comics.toServerSubData()
)

fun DomainThumbnail.toServerThumbnail(): ServerThumbnail = ServerThumbnail(
    extension,
    path
)

fun DomainSubData.toServerSubData(): ServerSubData = ServerSubData(
    available,
    collectionURI,
    items.map{ it.toServerItemData() },
    returned
)

fun DomainItemData.toServerItemData(): ServerItemData = ServerItemData(
    name,
    resourceURI,
    type
)


fun ServerResponse.toDomainResponse(): DomainResponse = DomainResponse(
    code,
    status,
    data.toDomainData()
)

fun ServerData.toDomainData(): DomainData = DomainData(
    count,
    limit,
    offset,
    results.map { it.toDomainHero() },
    total
)


fun ServerHero.toDomainHero(): DomainHero = DomainHero(
    id,
    name,
    description,
    modified,
    resourceURI,
    thumbnail.toDomainThumbnail(),
    comics.toDomainSubData()
)

fun ServerThumbnail.toDomainThumbnail(): DomainThumbnail = DomainThumbnail(
    extension,
    path
)

fun ServerSubData.toDomainSubData(): DomainSubData = DomainSubData(
    available,
    collectionURI,
    items.map { it.toDomainItemData() },
    returned
)

fun ServerItemData.toDomainItemData(): DomainItemData = DomainItemData(
    name,
    resourceURI,
    type
)

