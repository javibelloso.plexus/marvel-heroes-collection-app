package com.javibelloso.marvelheroescollection.api

import com.javibelloso.marvelheroescollection.api.model.ResponseHeroes
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("/v1/public/characters")
    suspend fun listHeroes(
        @Query("ts") ts: Long,
        @Query("apikey") apikey: String,
        @Query("hash") hash: String,
        @Query("offset") offset: Int,
        @Query("orderBy") orderBy: String = "name",
        @Query("limit") limit: Int = REQUEST_LIMIT
    ): ResponseHeroes

    @GET("/v1/public/characters/{characterId}")
    suspend fun getHeroDetailsById(
        @Path("characterId") characterId: Int,
        @Query("ts") ts: Long,
        @Query("apikey") apikey: String,
        @Query("hash") hash: String
    ): ResponseHeroes

}