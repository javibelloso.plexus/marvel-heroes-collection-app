package com.javibelloso.marvelheroescollection.api

import com.javibelloso.data.source.CredentialsDataSource
import com.javibelloso.marvelheroescollection.BuildConfig
import com.javibelloso.marvelheroescollection.common.md5
import javax.inject.Inject

class MarvelCredentialDataSource @Inject constructor() : CredentialsDataSource {
    override val timeStamp: Long
        get() = System.currentTimeMillis()
    override val publicKey: String
        get() = BuildConfig.PUBLIC_KEY
    override val privateKey: String
        get() = BuildConfig.PRIVATE_KEY
    override val hash: String
        get() = "$timeStamp$privateKey$publicKey".md5
}
