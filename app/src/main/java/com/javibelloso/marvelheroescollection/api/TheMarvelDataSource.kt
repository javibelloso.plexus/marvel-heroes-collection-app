package com.javibelloso.marvelheroescollection.api

import com.javibelloso.data.source.RemoteDataSource
import com.javibelloso.domain.HeroResult
import javax.inject.Inject

class TheMarvelDataSource @Inject constructor(
    private val api: ApiService,
    private val marvelCredentialDataSource: MarvelCredentialDataSource)
    : RemoteDataSource {

    override suspend fun getHeroesCollection(page: Int): List<HeroResult> {
        val ts = marvelCredentialDataSource.timeStamp
        val publicKey = marvelCredentialDataSource.publicKey
        val hash = marvelCredentialDataSource.hash
        val heroes = api.listHeroes(ts, publicKey, hash, page).data.results.map {
            it.toDomainHero() }
        return heroes
    }

    override suspend fun getDetailHero(id: Int): HeroResult {
        val ts = marvelCredentialDataSource.timeStamp
        val publicKey = marvelCredentialDataSource.publicKey
        val hash = marvelCredentialDataSource.hash
        val hero = api.getHeroDetailsById(id, ts, publicKey, hash).data.results[0].toDomainHero()
        return hero
    }

}