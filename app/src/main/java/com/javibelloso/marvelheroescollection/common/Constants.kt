package com.javibelloso.marvelheroescollection.api

const val URLBASE = "https://gateway.marvel.com"


// LIMIT OF HEROES TO REQUEST
const val DEFAULT_PAGE_INDEX = 1
const val VALUE_COUNT_PAGE = 36
const val REQUEST_LIMIT = 36

// EXCEPTIONS
const val SOCKET_NULL = "Socket is null"

// PING
const val PING_HOSTNAME = "8.8.8.8"
const val PING_PORT = 53
const val PING_TIMEOUT = 1500

// API

const val MARVEL_HEROES_COLLECTION_PUBLIC_KEY = "PUBLIC_KEY"
const val MARVEL_HEROES_COLLECTION_PRIVATE_KEY = "PRIVATE_KEY"
