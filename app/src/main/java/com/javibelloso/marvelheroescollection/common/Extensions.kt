package com.javibelloso.marvelheroescollection.common

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import com.bumptech.glide.Glide
import com.javibelloso.marvelheroescollection.R
import java.math.BigInteger
import java.security.MessageDigest


val String.md5: String
    get() {
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(this.toByteArray())).toString(16).padStart(32, '0')
    }


inline fun <reified T : Activity> Context.startActivity(vararg pairs: Pair<String, Any?>) {

    val intent = Intent(this, T::class.java)
        .apply {
            putExtras(bundleOf(*pairs))
        }
    startActivity(intent)
}


// load image with Glide
fun ImageView.loadImage(url: String, extension: String?) {
    Glide.with(this)
        .asBitmap()
        .load("$url.$extension")
        .centerCrop()
        .error(R.drawable.marvel)
        .into(this)
}

fun AlertDialog.Builder.showErrorDialog(error: String, action: ()-> Unit) {
        this.apply {
            setPositiveButton(R.string.ok) { dialog, id ->
                action.invoke()
            }
        }
        this.setTitle("Error")
        this.setMessage("Menssage: " + error)
        this.create()
}



