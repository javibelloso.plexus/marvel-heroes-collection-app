package com.javibelloso.domain

data class ResponseHeroesCollection(
    val code: Int,
    val status: String,
    val data: Data
)