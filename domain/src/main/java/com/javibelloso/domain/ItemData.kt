package com.javibelloso.domain

data class ItemData(
    val name: String,
    val resourceURI: String,
    val type: String?
)