package com.javibelloso.domain

data class Thumbnail(
    val extension: String,
    val path: String
)