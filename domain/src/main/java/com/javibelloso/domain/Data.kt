package com.javibelloso.domain

data class Data(
    val count: Int,
    val limit: Int,
    val offset: Int,
    val results: List<HeroResult>,
    val total: Int
)