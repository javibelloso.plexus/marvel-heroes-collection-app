import com.javibelloso.data.repository.MarvelRepository
import com.javibelloso.usecase.GetHeroes
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class GetHeroesTest {

    @Mock
    lateinit var marvelRepository: MarvelRepository

    lateinit var getHeroes: GetHeroes

    @Before
    fun setUp() {
        getHeroes = GetHeroes(marvelRepository)
    }

    @Test
    fun getRemoteHeroesAPI(): Unit = runBlocking {
        whenever(marvelRepository.getHeroesCollection(0)).thenReturn(mockedResponse.data.results)
        val result = getHeroes.run(0)
        assertEquals(result, mockedResponse.data.results)
    }

}