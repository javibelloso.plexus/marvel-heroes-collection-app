import com.javibelloso.data.repository.MarvelRepository
import com.javibelloso.usecase.GetHeroDetail
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class GetHeroDetailTest {

    @Mock
    lateinit var marvelRepository: MarvelRepository

    lateinit var getHeroDetail: GetHeroDetail


    @Before
    fun setUp() {
        getHeroDetail = GetHeroDetail(marvelRepository)
    }

    @Test
    fun getDetailHeroByIdApi(): Unit =
        runBlocking {
            whenever(marvelRepository.getHeroDetailById(0)).thenReturn(mockedHero)
            val result = getHeroDetail.run(0)
            Assert.assertEquals(mockedHero, result)
        }
    
}