package com.javibelloso.usecase

import com.javibelloso.data.repository.MarvelRepository
import com.javibelloso.domain.HeroResult

class GetHeroes (private val marvelRepository: MarvelRepository) {
    suspend fun run(page: Int): List<HeroResult> {
        return marvelRepository.getHeroesCollection(page)
    }
}