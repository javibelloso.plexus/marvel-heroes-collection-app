package com.test.usecase

import com.javibelloso.data.repository.NetworkRepository


class GetNetworkManager(private val networkRepository: NetworkRepository) {
    suspend fun run(lifecycle: Any, listener: (Boolean) -> Unit) =
        networkRepository.manageNetworkManager(lifecycle, listener)
}
