package com.javibelloso.usecase

import com.javibelloso.data.repository.MarvelRepository
import com.javibelloso.domain.HeroResult

class GetHeroDetail(private val marvelRepository: MarvelRepository) {
    suspend fun run(id: Int): HeroResult = marvelRepository.getHeroDetailById(id)
}